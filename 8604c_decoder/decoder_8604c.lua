
-- microcode implementation

-- lists of possible names in order
local addr_list = "0 ab cd xy hl vw ij pq st mst"
local reg_list  = "nul _ a b c d x y h l v w i j p q s t ms mt"
local op_list   = "nop _ _ _ _ add sub mul div us u2 or and xor mov"
local inc_list  = "load inc"
local flag_list = "nop _ _ _ iclr iset hclr hset"
local cond_list = "no yes"
-- name, bits, list of names for values 0-n
local ctrl_word_list = {
	{"addr"    , 4, addr_list},
	{"cond"    , 3, cond_list},
	{"1read1"  , 5, reg_list },
	{"1read2"  , 5, reg_list },
	{"memwrite", 5, reg_list },
	{"1op"     , 4, op_list  },
	{"2read1"  , 5, reg_list },
	{"2read2"  , 5, reg_list },
	{"1write"  , 5, reg_list },
	{"2op"     , 4, op_list  },
	{"2write"  , 5, reg_list },
	{"memread" , 5, reg_list },
	{"inc"     , 1, inc_list },
	{"flag"    , 3, flag_list},
}
-- order of names in instruction encoding
local instr_order = "1read1 1op 1read2 1write , 2read1 2op 2read2 2write , addr memread memwrite inc , cond , flag"
-- default for unspecified blocks
local instr_default = "nul nop nul nul , nul nop nul nul , pq nul nul load , no , nop"
-- table of mnem -> encoding, or list of encodings
local instr_list = {
	{"nop", ""},
	{"add", "a add b a ,,, yes"},
	{"sub", "a sub b a ,,, yes"},
	{"lod", {",, xy a nul inc", ""}},
	{"str", {",, xy nul a inc", ""}},
	{"mab", "a mov nul b ,,, yes"},
	{"mba", "b mov nul a ,,, yes"},
	{"jmp", "a mov nul i, b mov nul j, ab nul nul load , yes"},
	{"jmp imm16", {",, pq i nul inc", ",, pq j nul inc", "i mov nul p , j mov nul q , ij nul nul load, yes"}},
	{"ldi imm8", {",, pq a nul inc", ""}},
}


-- generate bit strings from each instr

require("util")

-- convert ctrl words from list into table
local ctrl_word_table = {}
local bit_pos = 1
for ctrll in each(ctrl_word_list) do
	local signame, numbits, ctrllist = unpack(ctrll)
	ctrl_word_table[signame] = {
		bitpos = bit_pos,
		numbits = numbits,
		ctrltable = mapt(flip(ctrllist:split(" ")), function(i) return i-1 end),
	}
	bit_pos = bit_pos + numbits
end
local num_bits = bit_pos-1

-- convert a cycle str into a table raw
local num_parts = #instr_order:split(",")
local function cycle_to_table(cycle)
	return pad(map(cycle:split(","), function(part)
		return part:gsub("^ +", ""):gsub(" +$", ""):split(" +")
	end), num_parts, {})
end
-- convert a cycle str into a table substituting in default values
local default_instr_table = cycle_to_table(instr_default)
local function cycle_to_table_expand(cycle)
	return mapi(cycle_to_table(cycle), function(part, partidx)
		local partt = (#part==0 and default_instr_table[partidx] or part)
		assert(#partt==#default_instr_table[partidx])
		return partt;
	end)
end
-- convert a cycle str into bits
local order_table = cycle_to_table(instr_order)
local function cycle_to_bits(cycle)
	local ctable = cycle_to_table_expand(cycle)
	local bittbl = {}
	map2(ctable, order_table, function(cpart, signalnames)
		map2(cpart, signalnames, function(sigvalname, signame)
			local ctrl = ctrl_word_table[signame]; assert(ctrl, "invalid signal name "..signame);
			local val = ctrl.ctrltable[sigvalname]; assert(val, "invalid value name "..sigvalname.." in signal "..signame);
			for i = 1, ctrl.numbits do
				bittbl[ctrl.bitpos+i-1] = bit.band(bit.rshift(val, i-1), 1)
			end
		end)
	end)
	assert(islist(bittbl), "bit list not a list")
	assert(#bittbl == num_bits, "bit list wrong number of bits ("..#bittbl.." should be "..num_bits..")")
	return bittbl
end
-- generate bit strings from all instrs
local instrs = {}
for instridx, instr in ipairs(instr_list) do
	local mnem, il = unpack(instr); assert(type(mnem)=="string", "mnem "..tostring(mnem).." is not a string");
	local ilt = type(il)=="table" and il or {il}
	local bitsl = {}
	for cycleidx, cycle in ipairs(ilt) do
		local bits = cycle_to_bits(cycle)
		--print(((cycleidx==1) and (mnem..(" "):rep(12-#mnem)) or (" "):rep(12)) .. cycleidx .. "   " .. table.concat(bits))
		bitsl[cycleidx] = bits
	end
	instrs[instridx] = {
		mnem = mnem,
		bitsl = bitsl,
		opcode = nil,
	}
end
-- generate instruction encoding
local opcode_bits = {}
local function opcode_cycle(code, i)
	assert(i>=1); if i==1 then return code else return 256 + ((code+i-1)%256) end
end
local function opcode_open(code, len)
	assert(code>=0 and code<256, "code out of range "..code)
	for i = 1, len do if opcode_bits[opcode_cycle(code, i)] then return false end end
	return true
end
local function opcode_insert(code, bitsl)
	for i = 1, #bitsl do local c = opcode_cycle(code, i); assert(not opcode_bits[c]); opcode_bits[c] = bitsl[i]; end
end
for instr in each(instrs) do
	for code = 0, 255 do
		if opcode_open(code, #instr.bitsl) then
			opcode_insert(code, instr.bitsl)
			instr.opcode = code
			break
		end
	end
	assert(instr.opcode, "could not encode "..instr.mnem)
end

for c = 0, 511 do
	if opcode_bits[c] then print(string.format("%03x", c), table.concat(opcode_bits[c])) end
end
