
if sc1i:
	sc1i:0
	sc1:t1
	addr = sc1
	read = t1
el sc2i:
	sc2i:0
	sc2:t2
	addr = sc2
	read = t2
el:
	if flg:
		set flags
	el alu:
	el cjp:
		read = instr
		if cond:
			p <- sc1 + sc2
			addr = sc1 + sc2
		el:
			addr = p++
	el str:
		addr = sc1
		write = sc2
		opr:flg
	end
	if flg or alu:
		addr = p++
		read = instr
	end
end
