
.def keyboard_page       #02
.def keyboard_head       #10
.def keyboard_buffer     #00
.def keyboard_buffer_len #10

.def program_start #1000

.org !program_start
	; keyboard driver
	keyboard_update:
		mov h !keyboard_page
		mov a [h, !keyboard_head]
		cmp a [z, :keyboard_tail]
		{
			
		jne }

.org $0000 ; zero page RAM
	keyboard_tail: byte
