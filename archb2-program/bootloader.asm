
; games to make:
;    one finger death punch
;    number invaders
;    kill the bit

.def addr_prog #0000

.def card_read  #0400
.def card_reset #0401

.def addr_dest #0100

.def out_7seg #0200

.loc addr_prog ; program ROM
	clx
	
	; reset card reader
	stx (card_reset)
	
	; load source addr into a, dest into s
	lda addr_dest
	sta s
	lda card_read
	
	; load card length
	ldx (a)
	jnz %lp_read_card
		jmp $addr_dest
	
	; copy card conents into dest
	lp_read_card:
		ldy (a)
		push y
		sty (out_7seg)
		dec
	jnz %lp_read_card
	
	jmp addr_dest

.loc #0140 ; just past bootloader ROM - alert if beyond bounds
	nop
