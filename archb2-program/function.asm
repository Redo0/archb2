
.def out_7seg #0200
.def addr_ram #0100

.loc addr_ram
	clx
	
	; increment counter and display it
	lp_increment:
		inc
		call $func_display
	jr %lp_increment
	
	; function to display data
	func_display:
		stx (out_7seg)
	jmp a
