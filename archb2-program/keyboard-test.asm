
.def out_7seg #0200
.def key_buffer #0300
.def key_buffer_mask #0F
.def key_head #0310

.loc #0100 ; RAM
	; y is tail
	ldy (key_head)
	
	lp_check_key:
		ldx (key_head)
		sub y
		jz %lp_check_key ; head = tail means keycode not available - keep checking
		
		; keycode available - read keycode from tail
		lda key_buffer
		ldx y
		adda x
		ldx (a)
		
		; write keycode to 7seg
		stx (out_7seg)
		
		; increment tail
		ldx y
		inc
		and key_buffer_mask
		ldy x
	jr %lp_check_key
