
.def ram_start #0100
.def mul_a #02
.def mul_b #05

.loc ram_start
; Set stack pointer
lda $stack_start
sta s

; args in x and z
ldx mul_b
ldz mul_a
call $func_mul
; result in x
stx (#0200)

halt:
jr %halt

func_mul:
	push a
	cla
	
	func_mul_lp:
		ldz x
		jz %func_mul_end
		glb
		jz %func_mul_skip
		ldx y
		adda x
		
		func_mul_skip:
		ldx y
		shl
		ldy x
		ldx z
		shr
		jr %func_mul_lp
		
	func_mul_end:
	ldx al
	
	pop a
jmp a ; Return

stack_start:
