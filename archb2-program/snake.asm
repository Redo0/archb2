
.def ram #0100

.def key_buffer #0300
.def key_head #0310

.def timer #0500

.def screen #0600

.loc ram
	
	
	; Move the snake head bit along one position, and die if it goes offscreen
	; bitX in z, posY in y
	; down = 0
	; left = 1
	; up = 2
	; right = 3
	advance_head:
		ldx ($snake_dir)
		jz %move_down
		dec
		jz %move_left
		dec
		jz %move_up
		
		; move bit or y depending on direction
		move_right:
			ldx z
			shr
			ldz x
		jr %move_done
		move_left:
			ldx z
			shl
			ldz x
		jr %move_done
		move_up:
			ldx y
			inc
			ldy x
		jr %move_done
		move_down:
			ldx y
			dec
			ldy x
		move_done:
		
		; lose if snake went offscreen
		ldx y
		and #07
		jz %lose_advance
		ldx z
		jz %lose_advance
		
	jmp a
	lose_advance:
		call $lose_snake
	
	lose_snake:
		ldx (timer)
		jr %lose_snake
	jmp a
	
	snake_dir: byte
	stack_start: byte[#20]

.loc #0180
	byte
