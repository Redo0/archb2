
.def out_7seg #0200
.def in_timer #0500

.loc #0100 ; RAM
	cly
	lp_count:
		; count 10 timer ticks
		ldx #0A
		lp_dec:
			ldz (in_timer)
			dec
		jnz %lp_dec
		
		; increment y and write to 7seg
		ldx y
		stx (out_7seg)
		inc
		ldy x
	jr %lp_count
