
-- ArchB2 Assembler
-- To use, run "luajit assembler.lua"
-- For how to use, run "luajit assembler.lua --help"


-----------------------------------------------------------

-- Load microcode definitions from same directory as this file
local cd = arg[0]:gsub("/", "\\"):match("(.+\\)[^\\]+$") or ""
dofile(cd.."ucode.lua")
local ucode = Ucode_process(dofile(cd.."ucode-archb2.lua"))

local opcodes_by_mnem = {}
for opcode = 0, 0xFF do
	local op = ucode[opcode]
	if op.mnemonic then
		if opcodes_by_mnem[op.mnemonic] then error("duplicate mnemonic "..op.mnemonic) end
		opcodes_by_mnem[op.mnemonic] = opcode
	end
end


-- Preprocess program to make it easier to parse

local string_escapes = {
	["\\"] = "\\",
	["n"] = "\n",
	["r"] = "\r",
	["t"] = "\t",
	["0"] = "\0",
	["\""] = "\"",
	["\'"] = "\'",
}
local function text_clean(text)
	local text2t = {}
	local len = #text
	local in_string = false
	local in_escape = false
	for i = 1, len do
		local c = text:sub(i, i)
		if not in_string then -- not in string
			if c=="\"" then
				in_string = true
				table.insert(text2t, "\n")
				table.insert(text2t, "#")
			else
				table.insert(text2t, c)
			end
		else -- in string
			if not in_escape then -- not in escape
				if c=="\"" then
					in_string = false
				elseif c=="\\" then
					in_escape = true
				else
					table.insert(text2t, string.format("%02X", string.byte(c)))
				end
			else -- in escape
				if string_escapes[c] then
					table.insert(text2t, string.format("%02X", string.byte(string_escapes[c])))
					in_escape = false
				else
					error("invalid escape sequence "..c)
				end
			end
		end
	end
	
	text = table.concat(text2t)
	
	text = text:gsub("\r", "")      -- strip CR
	text = text:gsub(";[^\n]+", "") -- strip comments
	text = text:gsub(",", "")       -- strip commas
	text = text:gsub("[ \t]+", " ") -- condense whitespace
	text = text:gsub(":", ":\n")    -- space out labels
	text = text:lower()
	return text
end


-- Assemble code into binary data

local function line_directive(text)
	if text:sub(1, 1)=="." then
		local name, rest = text:match("%.([a-z0-9_]+)[ \t]+(.+)$")
		return name, rest
	end
end
local function line_label(text)
	if text:sub(#text, #text)==":" then return text:sub(1, #text-1) end
end
local function line_mnem(text)
	return text
		:gsub("%#[0-9a-f][0-9a-f][0-9a-f][0-9a-f]", "imm16")
		:gsub("%#[0-9a-f][0-9a-f]", "imm8")
		:gsub("%$[0-9a-z_]+", "imm16")
		:gsub("%%[0-9a-z_]+", "imm8")
end
local function line_opcode(text)
	local mnem = line_mnem(text)
	local opcode = opcodes_by_mnem[mnem]
	if not opcode then error("no opcode for mnemonic "..mnem) end
	return opcode
end
local function line_data(text)
	if text:sub(1, 1)=="#" then
		local dtext = text:sub(2, #text):gsub(" ", "")
		if #dtext%2~=0 then error("odd number of digits in hex number") end
		if dtext:find("[^0-9a-fA-F]") then error("invalid character in hex number") end
		
		local data = {}
		for i = 1, #dtext, 2 do
			table.insert(data, tonumber(dtext:sub(i, i+1), 16))
		end
		return data
	end
end
local function line_space(text)
	local word = text:match("^[a-z]+")
	if word=="byte" or word=="word" then
		local len = (word=="word" and 2) or 1
		if text:find("%[") then
			local slen = text:match("%[%#([0-9a-f]+)%]")
			if not slen then error("invalid array size") end
			len = len * (tonumber(slen, 16) or error("invalid array size "..slen))
		end
		return len
	end
end
local function line_assembled_size(text)
	if line_directive(text) then -- directive
		return 0
	elseif line_label(text) then -- label
		return 0
	else
		local len = line_space(text)
		if len then -- spacer
			return len
		else
			local data = line_data(text)
			if data then -- string
				return #data
			else -- instruction
				local mnem = line_mnem(text)
				if mnem:find("imm16") then return 3
				elseif mnem:find("imm8") then return 2
				else return 1
				end
			end
		end
	end
end
local function assemble_line(text, addr, labels)
	if line_directive(text) then -- directive
		return {}
	elseif line_label(text) then -- label
		return {}
	elseif line_space(text) then
		return {}
	else -- instruction
		local data = line_data(text)
		if data then
			return data
		else
			local opcode = line_opcode(text)
			local ret = { opcode }
			local imm8 = text:match("%#([0-9a-f][0-9a-f])")
			if imm8 then
				table.insert(ret, tonumber(imm8, 16))
				local imm16 = text:match("%#[0-9a-f][0-9a-f]([0-9a-f][0-9a-f])")
				if imm16 then
					table.insert(ret, tonumber(imm16, 16))
				end
			else
				local lab8 = text:match("%%([0-9a-z_]+)")
				if lab8 then
					if not labels[lab8] then error("no label called "..lab8) end
					local diff = labels[lab8] - (addr+2)
					if diff>128 or diff<-127 then error("relative jump too large") end
					table.insert(ret, diff%256)
				else
					local lab16 = text:match("%$([0-9a-z_]+)")
					if lab16 then
						if not labels[lab16] then error("no label called "..lab16) end
						local val = labels[lab16]
						table.insert(ret, math.floor(val/256))
						table.insert(ret, val%256)
					end
				end
			end
			return ret
		end
	end
end
local function assemble_text(mem_taken, mem_data, text, filename)
	text = text_clean(text)
	
	local macros = {}
	text = text:gsub("%.def[ \t]+([a-z0-9_]+)[ \t]+([^\n]+)", function(name, val)
		if macros[name] then error("Macro already defined \""..name.."\"") end
		macros[name] = val
		return ""
	end)
	text = text:gsub("([a-z0-9_]+)", macros)
	
	local lines = {}
	for line in text:gmatch("[^\n]+") do
		line = line:gsub("^ +", "") -- trim leading space
		line = line:gsub(" +$", "") -- trim trailing space
		
		if line~="" then
			table.insert(lines, {text = line})
		end
	end
	
	local labels = {}
	local cur_addr = 0
	for line_idx, line in ipairs(lines) do
		line.addr = cur_addr
		local size = line_assembled_size(line.text)
		for i = 0, size-1 do
			if mem_taken[cur_addr+i] then error("reusing memory at address "..string.format("%04X", cur_addr+i)) end
			mem_taken[cur_addr+i] = true
		end
		cur_addr = cur_addr + size
		local dir, val = line_directive(line.text)
		if dir=="loc" then
			cur_addr = tonumber(val:sub(2, #val), 16) or error("invalid address \""..val.."\"")
		end
		local label = line_label(line.text)
		if label then
			if labels[label] then error("label already exists "..label) end
			labels[label] = cur_addr
		end
	end
	
	-- Output assembled text
	local asm_text = {}
	table.insert(asm_text, "Assembly of "..filename)
	--table.insert("Addr  Data      Assembly")
	for line_idx, line in ipairs(lines) do
		local data = assemble_line(line.text, line.addr, labels)
		
		for i, v in ipairs(data) do
			local addr = line.addr+i-1
			if mem_data[addr] then error("overwriting memory at address "..string.format("%04X", addr)) end
			mem_data[addr] = v
		end
		
		if #data>0 then
			local datastr = ""
			for i, v in ipairs(data) do
				datastr = datastr..string.format("%02X", v).." "
			end
			table.insert(asm_text, string.format("%04X", line.addr).."  "..datastr:sub(1, 8)..(" "):rep(8-#datastr).."  "..line.text)
		else
			table.insert(asm_text, "                "..line.text)
		end
	end
	
	return table.concat(asm_text, "\n")
end


-- Build punch card readable by events

local function build_card(mem_data, card_addr, card_size)
	local shifts = {}
	local last_pos = {0, 0, 0}
	local function add_brick(pos)
		table.insert(shifts, {
			pos[1]-last_pos[1],
			pos[2]-last_pos[2],
			pos[3]-last_pos[3],
		})
		last_pos = pos
	end
	
	local function encode_byte(data, addr)
		local parity_bit = false
		for i = 0, 7 do
			if bit.band(data, bit.lshift(1, i))~=0 then
				parity_bit = not parity_bit
				add_brick({
					8-i,
					-addr,
					0
				})
			end
		end
		if parity_bit then
			add_brick({
				0,
				-addr,
				0
			})
		end
	end
	
	local last_nonzero_addr = 0
	for mem_addr = card_addr, card_addr+card_size-1 do
		local addr = mem_addr - card_addr
		local data = mem_data[mem_addr]
		if data then
			encode_byte(data, addr+1)
			last_nonzero_addr = addr
		else
			encode_byte(0   , addr+1)
		end
	end
	encode_byte(last_nonzero_addr+1, 0)
	
	local odata = {}
	for shift_idx, shift in ipairs(shifts) do
		table.insert(odata, (-shift[2]).." "..(-shift[1]).." "..(shift[3]).."\n")
	end
	
	return table.concat(odata)
end


-- Output raw bin file

local function build_bin(mem_data, bin_addr, bin_size)
	local bin_data = {}
	for mem_addr = bin_addr, bin_addr+bin_size-1 do
		local val = mem_data[mem_addr] or 0
		--if val~=0 then print(mem_addr, string.format("%02X", val)) end
		table.insert(bin_data, string.char(val))
	end
	return table.concat(bin_data)
end


-- Print memory dump

local function build_dump(mem_data, dump_addr, dump_size)
	local dump_data = {}
	
	dump_addr = math.floor(dump_addr/0x10)*0x10
	dump_size = math.ceil (dump_size/0x10)*0x10
	local dump_stop = dump_addr + dump_size - 0x10
	
	table.insert(dump_data, "Memory Dump:")
	
	--table.insert(dump_data, "Addr  ASCII             Hex")
	local last_printed_addr = -0x10
	for addr_base = dump_addr, dump_stop, 0x10 do
		local line_empty = true
		
		local str_data = ""
		local str_char = ""
		
		for addr = addr_base, addr_base+0xF do
			local v = mem_data[addr] or 0
			if v~=0 then line_empty = false end
			
			str_data = str_data..string.format("%02X", v).." "
			if addr%8==7 then str_data = str_data.." " end
			
			if v>=0x20 and v<0x7E then
				str_char = str_char..string.char(v)
			else
				str_char = str_char.."."
			end
		end
		
		if (not line_empty) or addr_base==dump_addr or addr_base==dump_stop then
			if last_printed_addr~=addr_base-0x10 then
				table.insert(dump_data, "...")
			end
			table.insert(dump_data, string.format("%04X", addr_base).."  "..str_char.."  "..str_data)
			last_printed_addr = addr_base
		end
	end
	
	return table.concat(dump_data, "\n")
end


--------------------------------------------------------

-- Read flags from command line input

local out_format = nil
local out_start = 0x0000
local out_length = 0x10000
local out_length_default = true
local in_filenames = {}
local out_filenames = {}
local quiet_mode = false

local arg_flags

local function show_help()
	print("ArchB2 Assembler Help")
	print("Flags:")
	for f_idx, flag in ipairs(arg_flags) do
		print("    "..flag[1].."  "..flag[2]..(" "):rep(12-#flag[2]).."  "..flag[4]:gsub("\n", "\n"..(" "):rep(24)))
	end
end
local function set_in_file(fn)
	table.insert(in_filenames, fn)
end
local function set_out_file(fn)
	table.insert(out_filenames, {
		fn,
		out_start,
		out_length,
		out_length_default
	})
end
local function set_start(start)
	out_start = tonumber(start)
	if not out_start then error("Invalid number "..start) end
end
local function set_length(len)
	out_length = tonumber(len)
	out_length_default = false
	if not out_length then error("Invalid number "..len) end
end
local function set_quiet()
	quiet_mode = true
end

arg_flags = {
	-- shortcut, flag name, args, default, description, function
	{"-h", "--help"    , 0, "Show this menu.", show_help},
	{"-q", "--quiet"   , 0, "Suppress assembled data and memory dump output.", set_quiet},
	{"-i", "--in-file" , 1, "File to assemble.\nMultiple -i flags are allowed, to put multiple assembled files into one binary.", set_in_file},
	{"-o", "--out-file", 1, "File to write the output data to.\nMultiple output files are allowed.\nEach will use the last address options specified before its flag.\nAcceptable extensions:\n.bin - Output raw binary file.\n.txt - Output a punch card data file.", set_out_file},
	{"-s", "--start"   , 1, "Address to start the output at. \nDefault is 0. Accepts decimal and hex numbers.", set_start},
	{"-l", "--length"  , 1, "Length of data to output. Default is 0x10000 - start address.", set_length},
}

local arg_idx = 1
while arg_idx <= #arg do
	local a = arg[arg_idx]
	local flag_hit
	local fargs = {}
	for f_idx, flag in ipairs(arg_flags) do
		if a==flag[1] or a==flag[2] then
			flag_hit = flag
			for i = 1, flag[3] do
				arg_idx = arg_idx + 1
				if arg_idx > #arg then error("Not enough arguments") end
				table.insert(fargs, arg[arg_idx])
			end
		end
	end
	arg_idx = arg_idx + 1
	if not flag_hit then error("invalid argument "..a) end
	flag_hit[5](unpack(fargs))
end


---------------------------------------------


-- Assemble all input files into data array

local mem_taken = {}
local mem_data = {}

for i, ifilename in ipairs(in_filenames) do
	local ifile = io.open(ifilename, "rb")
	if not ifile then error("could not open "..ifilename.." for reading") end
	local itext = ifile:read("*a")
	ifile:close()
	
	local asm = assemble_text(mem_taken, mem_data, itext, ifilename:match("[^\\/]+$"))
	if not quiet_mode then print(asm) print() end
end


-- Print memory dump

if not quiet_mode then
	local dump = build_dump(mem_data, 0x0000, 0x10000)
	print(dump)
	print()
end


-- Output all output files

for i, output in ipairs(out_filenames) do
	local start = output[2]
	local len = output[4] and (0x10000 - start) or output[3]
	if start<0 or start>0xFFFF then error("invalid start location "..string.format("%04X", start).." (must be between 0 and 0xFFFF)") end
	if len<0 or len+start-1>0xFFFF then error("invalid length "..string.format("%04X", len).." (must be between 0 and 0x10000-start)") end
	
	local ofilename = output[1]
	local format = ofilename:match("%.[^%.]+$")
	local out_data
	local data_name = nil
	if format==".txt" then
		out_data = build_card(mem_data, start, len)
		data_name = "card"
	elseif format==".bin" then
		out_data = build_bin (mem_data, start, len)
		data_name = "bin"
	else
		error("Invalid output format "..(format or ""))
	end
	
	if out_data then
		local ofile = io.open(ofilename, "wb")
		if not ofile then error("could not open "..ofilename.." for writing") end
		ofile:write(out_data)
		ofile:close()
	end
	
	if not quiet_mode then
		print(string.format("Output %s data for range %04X to %04X to %s", data_name, start, start+len-1, ofilename))
	end
end
