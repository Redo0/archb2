
#include <stdio.h>
#include "cvector.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

typedef enum {false, true} bool;

#define asm_error(err, ...) { \
	printf("Error (Line %i): " err "\n", cur_line, __VA_ARGS__); \
}

bool str_equal(char* s1, char* s2) {
	for(; *s2; s1++, s2++) {
		if(*s1 != *s2) return false;
	}
	if(*s1 != *s2) return false;
	return true;
}


// Parser

typedef enum {
	S_OPEN,
	S_STRING,
	S_STRING_ESCAPE,
	S_STRING_HEX_FIRST,
	S_STRING_HEX_SECOND,
	S_NAME,
	S_NUMBER_HEX_FIRST,
	S_NUMBER_HEX_SECOND,
	S_COMMENT,
} parser_state;

typedef enum {
	T_BREAK,
	T_NAME,
	T_DIRECTIVE,
	T_LABEL,
	T_BRACE_OPEN,
	T_BRACE_CLOSE,
	T_PAREN_OPEN,
	T_PAREN_CLOSE,
	T_GLOBAL,
	T_LOCAL,
	T_NUMBER,
	T_STRING,
} parser_token_type;

typedef struct {
	parser_token_type type;
	cvector_vector_type(char) data;
	int line;
	char* typename;
} parser_token;

bool p_is_whitespace  (char c) { return c=='\r' || c=='\n' || c=='\t' || c==' ' || c==',' || c==EOF; }
bool p_is_newline     (char c) { return c=='\r' || c=='\n'; }
bool p_is_name        (char c) { return (c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9') || (c=='_'); }
bool p_is_number      (char c) { return (c>='a' && c<='f') || (c>='A' && c<='F') || (c>='0' && c<='9'); }
bool p_is_directive   (char c) { return c=='.'; }
bool p_is_label       (char c) { return c==':'; }
bool p_is_brace_open  (char c) { return c=='{'; }
bool p_is_brace_close (char c) { return c=='}'; }
bool p_is_paren_open  (char c) { return c=='('; }
bool p_is_paren_close (char c) { return c==')'; }
bool p_is_number_start(char c) { return c=='#'; }
bool p_is_local       (char c) { return c=='%'; }
bool p_is_global      (char c) { return c=='$'; }
bool p_is_quote       (char c) { return c=='\"'; }
bool p_is_escape      (char c) { return c=='\\'; }
bool p_is_hex_escape  (char c) { return c=='x'; }
bool p_is_comment     (char c) { return c==';'; }
char hex_to_number(int cur_line, char c) {
	if(c>='0' && c<='9') {
		return c-'0';
	} else if(c>='a' && c<='f') {
		return c-'a'+10;
	} else if(c>='A' && c<='F') {
		return c-'A'+10;
	} else {
		asm_error("Invalid hex digit %c", c);
		return 0;
	}
}
char string_escape(int cur_line, char c) {
	switch(c) {
		case '\\': return '\\';
		case '\"': return '\"';
		case '\'': return '\'';
		case 'r' : return '\r';
		case 'n' : return '\n';
		case '0' : return '\0';
		case 't' : return '\t';
	}
	asm_error("Invalid escape character %c", c);
	return 0;
}

#define p_push_token(ttype) { \
	if(pdata) cvector_push_back(pdata, '\0'); \
	parser_token tok = { \
		.type = (ttype), \
		.data = pdata, \
		.line = cur_line, \
		.typename = #ttype \
	}; \
	cvector_push_back(ptokens, tok); \
	pdata = NULL; \
}
#define p_push_data(td) cvector_push_back(pdata, (td));
#define p_set_state(ts) pstate = (ts);
#define p_rewind() i--;
#define p_last_token() ptokens[cvector_size(ptokens)-1]

cvector_vector_type(parser_token) parse(cvector_vector_type(char) fchars) {
	cvector_vector_type(parser_token) ptokens = NULL;
	
	parser_state pstate = S_OPEN;
	cvector_vector_type(char) pdata = NULL;
	char pdata2;
	
	int cur_line = 0;
	p_push_token(T_BREAK);
	
	for(int i=0; i<cvector_size(fchars); i++) {
		char c = fchars[i];
		
		if(c=='\n') cur_line++;
		
		switch(pstate) {
			case S_OPEN: {
				if(p_is_newline(c) && p_last_token().type != T_BREAK) { p_push_token(T_BREAK); }
				else if(p_is_whitespace(c));
				else if(p_is_name(c)) {
					p_set_state(S_NAME);
					p_push_data(c);
				}
				else if(p_is_number_start(c)) { p_set_state(S_NUMBER_HEX_FIRST); }
				else if(p_is_quote       (c)) { p_set_state(S_STRING          ); }
				else if(p_is_comment     (c)) { p_set_state(S_COMMENT         ); }
				else if(p_is_directive   (c)) { p_push_token(T_DIRECTIVE  ); }
				else if(p_is_local       (c)) { p_push_token(T_LOCAL      ); }
				else if(p_is_global      (c)) { p_push_token(T_GLOBAL     ); }
				else if(p_is_brace_open  (c)) { p_push_token(T_BRACE_OPEN ); }
				else if(p_is_brace_close (c)) { p_push_token(T_BRACE_CLOSE); }
				else if(p_is_paren_open  (c)) { p_push_token(T_PAREN_OPEN ); }
				else if(p_is_paren_close (c)) { p_push_token(T_PAREN_CLOSE); }
				else if(p_is_label       (c)) { p_push_token(T_LABEL      ); p_push_token(T_BREAK); }
				else { asm_error("Unexpected character %c in open", c); }
			} break;
			case S_STRING: {
				if(p_is_quote(c)) {
					p_push_token(T_STRING);
					p_set_state(S_OPEN);
				} else if(p_is_escape(c)) {
					p_set_state(S_STRING_ESCAPE);
				} else {
					p_push_data(c);
				}
			} break;
			case S_STRING_ESCAPE: {
				if(p_is_hex_escape(c)) {
					p_set_state(S_STRING_HEX_FIRST);
				} else {
					p_push_data(string_escape(cur_line, c));
					p_set_state(S_STRING);
				}
			} break;
			case S_STRING_HEX_FIRST: {
				if(p_is_number(c)) {
					pdata2 = hex_to_number(cur_line, c);
				} else {
					asm_error("Unexpected character %c in hex escape", c);
					pdata2 = 0;
				}
				p_set_state(S_STRING_HEX_SECOND);
			} break;
			case S_STRING_HEX_SECOND: {
				if(p_is_number(c)) {
					p_push_data((pdata2*16) + hex_to_number(cur_line, c));
				} else {
					asm_error("Unexpected character %c in hex escape", c);
					p_push_data(0);
				}
				p_set_state(S_STRING);
			} break;
			case S_NAME: {
				if(p_is_name(c)) {
					p_push_data(c);
				} else {
					p_push_token(T_NAME);
					p_set_state(S_OPEN);
					p_rewind();
				}
			} break;
			case S_NUMBER_HEX_FIRST: {
				if(p_is_number(c)) {
					pdata2 = hex_to_number(cur_line, c);
					p_set_state(S_NUMBER_HEX_SECOND);
				} else {
					p_push_token(T_NUMBER);
					p_set_state(S_OPEN);
					p_rewind();
				}
			} break;
			case S_NUMBER_HEX_SECOND: {
				if(p_is_number(c)) {
					p_push_data((pdata2*16) + hex_to_number(cur_line, c));
				} else {
					asm_error("Unexpected character %c in hex number", c);
					p_push_data(0);
				}
				p_set_state(S_NUMBER_HEX_FIRST);
			}
			case S_COMMENT: {
				if(p_is_newline(c)) {
					p_set_state(S_OPEN);
					p_rewind();
				}
			}
		}
	}
	
	cvector_free(pdata);
	return ptokens;
}


// Lexer

typedef enum {
	LT_INSTRUCTION,
	LT_DATA,
	LT_DEFINE,
	LT_LABEL,
	LT_LOCATION,
} lexer_token_type;

typedef enum {
	LS_OPEN,
	LS_DIRECTIVE_NAME,
	LS_MACRO_NAME,
	LS_MACRO_DEFINITION,
	LS_LOCATION,
	LS_AFTER_NAME,
	LS_INSTRUCTION,
} lexer_state_type;

typedef struct {
	lexer_token_type type;
	cvector_vector_type(parser_token) data;
	int line;
	char* typename;
} lexer_token;

#define l_push_token(ttype) { \
	lexer_token tok = { \
		.type = (ttype), \
		.data = ldata, \
		.line = cur_line, \
		.typename = #ttype \
	}; \
	cvector_push_back(ltokens, tok); \
	ldata = NULL; \
	printf("%s\n", #ttype); \
}
#define l_push_data(td) cvector_push_back(ldata, (td));
#define l_set_state(ts) lstate = (ts);
#define l_rewind() i--;
#define l_last_token() ptokens[cvector_size(ltokens)-1]

cvector_vector_type(lexer_token) lex(cvector_vector_type(parser_token) ptokens) {
	cvector_vector_type(lexer_token) ltokens = NULL;
	
	lexer_state_type lstate = LS_OPEN;
	cvector_vector_type(parser_token) ldata = NULL;
	
	int cur_line = 0;
	for(int i=0; i<cvector_size(ptokens); i++) {
		parser_token ptoken = ptokens[i];
		cur_line = ptoken.line;
		
		switch(lstate) {
			case LS_OPEN: {
				switch(ptoken.type) {
					case T_BREAK: break;
					case T_NAME: {
						l_push_data(ptoken);
						l_set_state(LS_AFTER_NAME);
					} break;
					case T_DIRECTIVE: {
						l_set_state(LS_DIRECTIVE_NAME);
					} break;
					default: { asm_error("Unexpected token %s in open", ptoken.typename); } break;
				}
			} break;
			case LS_DIRECTIVE_NAME: {
				if(ptoken.type==T_NAME) {
					if(str_equal(ptoken.data, "loc")) {
						l_set_state(LS_LOCATION);
					} else if(str_equal(ptoken.data, "def")) {
						l_set_state(LS_MACRO_NAME);
					} else {
						asm_error("Unrecognized directive %s", ptoken.data);
						l_set_state(LS_OPEN);
					}
				} else {
					asm_error("Unexpected token %s in directive name", ptoken.typename);
					l_set_state(LS_OPEN);
				}
			} break;
			case LS_MACRO_NAME: {
				if(ptoken.type==T_NAME) {
					l_push_data(ptoken);
					l_set_state(LS_MACRO_DEFINITION);
				} else {
					asm_error("Unexpected token %s in macro name", ptoken.typename);
					l_set_state(LS_OPEN);
				}
			} break;
			case LS_MACRO_DEFINITION: {
				if(ptoken.type==T_BREAK) {
					l_push_token(LT_DEFINE);
				} else {
					l_push_data(ptoken);
					l_set_state(LS_OPEN);
				}
			} break;
			case LS_LOCATION: {
				
			} break;
			case LS_AFTER_NAME: {
				if(ptoken.type==T_LABEL) {
					l_push_token(LT_LABEL);
					l_set_state(LS_OPEN);
				} else {
					l_set_state(LS_INSTRUCTION);
				}
			} break;
			case LS_INSTRUCTION: {
				switch(ptoken.type) {
					case T_BREAK: {
						l_push_token(LT_INSTRUCTION);
						l_set_state(LS_OPEN);
					} break;
					case T_BRACE_OPEN: {
						
					} break;
					case T_BRACE_CLOSE: {
						
					} break;
					default: { l_push_data(ptoken); } break;
				}
			} break;
		}
	}
	return 0;
}


// Main

int main(int argc, char** argv) {
	char* filename_in = (argc > 1) ? (argv[0]) : ("test_code.asm");
	FILE* file_in = fopen(filename_in, "r");
	if(!file_in) return 0;
	
	cvector_vector_type(char) fchars = NULL;
	
	while(!feof(file_in)) {
		char c = fgetc(file_in);
		cvector_push_back(fchars, c);
	}
	fclose(file_in);
	
	printf("Parser\n");
	cvector_vector_type(parser_token) ptokens = parse(fchars);
	printf("Lexer\n");
	cvector_vector_type(lexer_token) ltokens = lex(ptokens);
	
	return 0;
}
