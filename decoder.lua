
require("ucode")
local ucode = Ucode_process(require("ucode-archb2"))

local shifts = {}
local last_pos = {0, 0, 0}
local function add_brick(pos)
	table.insert(shifts, {
		pos[1]-last_pos[1],
		pos[2]-last_pos[2],
		pos[3]-last_pos[3],
	})
	last_pos = pos
end
for opcode = 0, 0xFF do
	local code = ucode[opcode]
	
	local opcode_enc = Reverse_8bit(opcode)
	--local opcode_enc = opcode
	
	for sig_idx, sig in ipairs(code) do
		local pos = {
			opcode_enc%16,
			math.floor(opcode_enc/16),
			-sig*2,
		}
		add_brick(pos)
	end
end

local ofile = io.open("I:/data/games/blockland/client/blockland5/Blockland/Add-Ons/Client_ExportData/input2.txt", "wb")
for shift_idx, shift in ipairs(shifts) do
	ofile:write((-shift[2]).." "..(-shift[1]).." "..(shift[3]).."\n")
end
ofile:close()
print("decoder data generated - execute client_build_rom(); in blockland to build")
