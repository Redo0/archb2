
; bootloader
; load program from card reader, write to ram

.def card_reset    #0300
.def card_read     #0301
.def card_ready    #0302

.def program_start #0000
.def program_end   #0100
.def dest_start    #0100

.def card_size     #40

.loc #2000
word[#08] ; uninitialized data
test_string: "Hello, World\r\n\0"
" 
this is still a string
" "\0"

.loc program_start
	; reset card reader
	ldx #01
	stx (card_reset)
	
	; load initial conditions
	ldz card_size  ; counter
	lda dest_start ; destination
	sta s
	
	stx (#0200)
	
	; read each byte on card sequentially into RAM
	lp_card_copy:
		; wait for card to be ready
		lp_card_wait:
			ldx (card_ready)
		jz %lp_card_wait
		
		ldx (card_ready)
		ldy (card_read)  ; load byte from card
		push y           ; save into RAM
		ldx z            ; decrement counter to done
		dec
		ldz x
	jnz %lp_card_copy
	
	; execute loaded program
	jmp dest_start
.loc program_end
	byte[#100]
