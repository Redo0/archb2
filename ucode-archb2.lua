
-- error on access to undefined variables

assert(getmetatable(_G)==nil, "_G already has a metatable")
setmetatable(_G, {
	__index = function(t, i) error("attempt to access nil variable "..i, 2) end
})


-- raw architecture signals

local HaltFlagBit0  = 0
local IntFlagBit0   = 1
local CondSel0      = 3
local CondSel1      = 4
local CondCheck     = 5
local AddrAddCond   = 6
local AddrAdd       = 7
local AddrLeftSel   = 8
local AddrInSel     = 9
local WriteP        = 10
local AddrCin       = 11
local AddrRightSel0 = 12
local AddrRightSel1 = 13
local AddrRightSel2 = 14
local AddrMemSel0   = 15
local AddrMemSel1   = 16
local AddrLeftWSel0 = 17
local AddrLeftWSel1 = 18
local AddrLeftWSel2 = 19
local AluInSel0     = 20
local AluInSel1     = 21
local AluInSel2     = 22
local AluInSel3     = 23
local AluOutWSel0   = 24
local AluOutWSel1   = 25
local AluOutWSel2   = 26
local AluInv        = 27
local AluShiftSel0  = 28
local AluShiftSel1  = 29
local AluShiftSel2  = 30
local AluCinSel0    = 31
local AluCinSel1    = 32
local AluOutSel0    = 33
local AluOutSel1    = 34
local AluOutSel2    = 35
local LdR           = 36
local MemModeSel0   = 37
local MemModeSel1   = 38


-- signal selections

local AddrLeftAddrResult = {           }
local AddrLeftIAluOut    = {AddrLeftSel}

local AddrRight0         = {               AddrRightSel1, AddrRightSel0}
local AddrRightV         = {AddrRightSel2,                             }
local AddrRightT         = {AddrRightSel2,                AddrRightSel0}
local AddrRightS         = {AddrRightSel2, AddrRightSel1               }
local AddrRightA         = {AddrRightSel2, AddrRightSel1, AddrRightSel0}

local AddrMem0           = {             AddrMemSel0}
local AddrMemAddrResult  = {AddrMemSel1             }
local AddrMemAddrRight   = {AddrMemSel1, AddrMemSel0}

local AddrLeftWNone      = {                                           }
local AddrLeftWA         = {AddrLeftWSel2                              }
local AddrLeftWS         = {AddrLeftWSel2,                AddrLeftWSel0}
local AddrLeftWT         = {AddrLeftWSel2, AddrLeftWSel1               }
local AddrLeftWV         = {AddrLeftWSel2, AddrLeftWSel1, AddrLeftWSel0}

local AluInM2            = {           AluInSel2,            AluInSel0}
local AluInM1            = {           AluInSel2, AluInSel1           }
local AluIn0             = {           AluInSel2, AluInSel1, AluInSel0}
local AluIn1             = {AluInSel3                                 }
local AluIn2             = {AluInSel3,                       AluInSel0}
local AluInAL            = {AluInSel3,            AluInSel1           }
local AluInAH            = {AluInSel3,            AluInSel1, AluInSel0}
local AluInI             = {AluInSel3, AluInSel2                      }
local AluInZ             = {AluInSel3, AluInSel2,            AluInSel0}
local AluInY             = {AluInSel3, AluInSel2, AluInSel1           }
local AluInX             = {AluInSel3, AluInSel2, AluInSel1, AluInSel0}

local AluOutWNone        = {                                     }
local AluOutWX           = {AluOutWSel2                          }
local AluOutWY           = {AluOutWSel2,              AluOutWSel0}
local AluOutWZ           = {AluOutWSel2, AluOutWSel1             }
local AluOutWI           = {AluOutWSel2, AluOutWSel1, AluOutWSel0}

local AluShift0          = {                                        }
local AluShiftX          = {              AluShiftSel1              }
local AluShiftXShl1      = {              AluShiftSel1, AluShiftSel0}
local AluShiftXRol1      = {AluShiftSel2                            }
local AluShiftXShr1      = {AluShiftSel2,               AluShiftSel0}
local AluShiftXRor1      = {AluShiftSel2, AluShiftSel1              }
local AluShiftXShra1     = {AluShiftSel2, AluShiftSel1, AluShiftSel0}

local AluCinC            = {            AluCinSel0}
local AluCin0            = {AluCinSel1            }
local AluCin1            = {AluCinSel1, AluCinSel0}

local AluOutMemDataOut   = {            AluOutSel1, AluOutSel0}
local AluOutAnd          = {AluOutSel2                        }
local AluOutOr           = {AluOutSel2,             AluOutSel0}
local AluOutXor          = {AluOutSel2, AluOutSel1            }
local AluOutAddSub       = {AluOutSel2, AluOutSel1, AluOutSel0}

local MemModeNone        = {                        }
local MemModeRead        = {             MemModeSel0}
local MemModeWrite       = {MemModeSel1             }

local CondZ              = {                  }
local CondG              = {          CondSel0}
local CondL              = {CondSel1          }
local CondC              = {CondSel1, CondSel0}

local AddrInAddrRight    = {         }
local AddrInP            = {AddrInSel}


-- selection descriptions

-- AddrRight: 0, P, V, T, S, A
-- AddrIn opt: AddrRight, P
-- AddrAdd opt: 0, 1
-- AddrCin opt: 0, 1
-- AddrLeft: AddrResult, P, IAluOut
-- AddrLeftW opt: None, A, S, T, V
-- WriteP opt: 0, 1

-- AddrMem: 0, AddrResult, AddrRight
-- MemMode opt: None, Read, Write
-- LdR opt: 0, 1

-- AluIn: 0, 1, 2, AL, AH, I, Z, Y, X
-- AluInv opt: 0, 1
-- AluShift opt: 0, X, XShl1, XRol1, XShr1, XRor1, XShra1
-- AluCin opt: 0, 1, C
-- AluOut: MemDataOut, And, Or, Xor, AddSub
-- AluOutW opt: None, X, Y, Z, I

-- AddrAddCond opt: 0, 1
-- Cond opt: Z, G, L, C

-- high-level operations

local LoadNextInstr = {AddrInP, AddrCin, AddrMemAddrResult, WriteP, MemModeRead, LdR}
local IncInstr      = {}

local AluMove  = {AluShift0, AluOutOr}
local AluAdd   = {AluShiftX, AluOutWX, AluOutAddSub}
local AluSub   = {AluShiftX, AluOutWX, AluOutAddSub, AluCin1, AluInv}
local AluAddc  = {AluShiftX, AluOutWX, AluOutAddSub, AluCinC        }
local AluSubc  = {AluShiftX, AluOutWX, AluOutAddSub, AluCinC, AluInv}
local AluAnd   = {AluShiftX, AluOutWX, AluOutAnd        }
local AluAndn  = {AluShiftX, AluOutWX, AluOutAnd, AluInv}
local AluOr    = {AluShiftX, AluOutWX, AluOutOr         }
local AluXor   = {AluShiftX, AluOutWX, AluOutXor        }
local AluShift = {AluOutWX, AluIn0, AluOutOr}

local LoadImm    = {AddrInP, AddrCin, AddrMemAddrResult, WriteP, MemModeRead, AluOutMemDataOut}
local LoadImm8   = {LoadImm, AluOutWI}
local LoadImm16  = {LoadImm, AddrLeftIAluOut, AddrLeftWT}
local LoadImm16A = {LoadImm, AddrLeftIAluOut, AddrLeftWA}

local Pop8Bit = {AddrRightS, AddrInAddrRight, AluInM1, AddrAdd, AddrLeftAddrResult, AddrLeftWS, AddrMemAddrResult, MemModeRead, AluOutMemDataOut}
local PopI    = {Pop8Bit, AluOutWI}
local Pop2I   = {AddrRightS, AddrInAddrRight, AluInM2, AddrAdd, AddrLeftAddrResult, AddrLeftWS, AddrMemAddrResult, MemModeRead, AluOutMemDataOut, AluOutWI}
local Pop216  = {AddrRightS, AddrInAddrRight, AddrCin,          AddrLeftIAluOut,                AddrMemAddrResult, MemModeRead, AluOutMemDataOut          }

local MemOff      = {MemModeNone, AddrMem0}
local AddrBusMove = {MemOff, AddrInAddrRight, AddrLeftAddrResult}

local LoadFromA            = {AddrRightA, AddrInAddrRight, AddrMemAddrResult, MemModeRead, AluOutMemDataOut}
local LoadFromAPlus1       = {LoadFromA, AddrCin}
local LoadFromT            = {AddrRightT, AddrInAddrRight, AddrMemAddrResult, MemModeRead, AluOutMemDataOut}
local LoadFromSOffset      = {AddrRightS, AddrInAddrRight, AluInI, AddrAdd, AddrMemAddrResult, MemModeRead, AluOutMemDataOut}
local LoadFromTPlus1       = {LoadFromT, AddrCin}
local LoadFromSOffsetPlus1 = {AddrRightS, AddrInAddrRight, AluInI, AddrAdd, AddrMemAddrResult, MemModeRead, AluOutMemDataOut}

local Store8Bit           = {AluMove, MemModeWrite}
local StoreToA            = {AddrRightA, AddrInAddrRight, AddrMemAddrResult, Store8Bit}
local StoreToT            = {AddrRightT, AddrInAddrRight, AddrMemAddrResult, Store8Bit}
local StoreToSOffset      = {AddrRightS, AddrInAddrRight, AluInI, AddrAdd, AddrMemAddrResult, Store8Bit}
local Push8Bit            = {AddrRightS, AddrInAddrRight, AddrCin, AddrMemAddrRight, AddrLeftAddrResult, AddrLeftWS, Store8Bit}
local StoreToTPlus1       = {StoreToT, AddrCin}
local StoreToSOffsetPlus1 = {StoreToSOffset, AddrCin}
local OffsetSToT          = {AddrRightS, AddrInAddrRight, AluInI, AddrAdd, AddrLeftAddrResult, AddrLeftWS}

local JumpLong         = {AddrMemAddrRight, WriteP, MemModeRead, LdR}
local SaveReturnA      = {AddrInP, AddrCin, AddrLeftAddrResult, AddrLeftWA}
local JumpRelative     = {AddrInP, AddrCin, AluInI, AddrAdd    , AddrMemAddrResult, WriteP, MemModeRead, LdR}
local JumpRelativeCond = {AddrInP, AddrCin, AluInI, AddrAddCond, AddrMemAddrResult, WriteP, MemModeRead, LdR}
local SaveReturnV      = {AddrInP,          AddrLeftAddrResult, AddrLeftWV}
local SaveReturnVInc   = {AddrInP, AddrCin, AddrLeftAddrResult, AddrLeftWV}

-- microcode ops

local ucode = {
	-- reset
	[0x00] = {"rst", AddrRight0, AddrMemAddrResult, WriteP, MemModeRead, LdR}, --V
	
	-- register moves
	[0x01] = {"clx"   , LoadNextInstr, AluIn0 , AluMove, AluOutWX }, --V
	[0x02] = {"ldx y" , LoadNextInstr, AluInY , AluMove, AluOutWX }, --V
	[0x03] = {"ldx z" , LoadNextInstr, AluInZ , AluMove, AluOutWX }, --V
	[0x04] = {"ldx al", LoadNextInstr, AluInAL, AluMove, AluOutWX },
	[0x05] = {"ldx ah", LoadNextInstr, AluInAH, AluMove, AluOutWX },
	[0x06] = {"cly"   , LoadNextInstr, AluIn0 , AluMove, AluOutWY },
	[0x07] = {"ldy x" , LoadNextInstr, AluInX , AluMove, AluOutWY }, --V
	[0x08] = {"ldy z" , LoadNextInstr, AluInZ , AluMove, AluOutWY },
	[0x09] = {"ldy al", LoadNextInstr, AluInAL, AluMove, AluOutWY },
	[0x0A] = {"ldy ah", LoadNextInstr, AluInAH, AluMove, AluOutWY },
	[0x0B] = {"clz"   , LoadNextInstr, AluIn0 , AluMove, AluOutWZ },
	[0x0C] = {"ldz x" , LoadNextInstr, AluInX , AluMove, AluOutWZ }, --V
	[0x0D] = {"ldz y" , LoadNextInstr, AluInY , AluMove, AluOutWZ },
	[0x0E] = {"ldz al", LoadNextInstr, AluInAL, AluMove, AluOutWZ },
	[0x0F] = {"ldz ah", LoadNextInstr, AluInAH, AluMove, AluOutWZ },
	
	-- alu ops reg
	[0x10] = {"inc"   , LoadNextInstr, AluIn1, AluAdd }, --V
	[0x11] = {"add y" , LoadNextInstr, AluInY, AluAdd },
	[0x12] = {"add z" , LoadNextInstr, AluInZ, AluAdd }, --V
	[0x13] = {"dec"   , LoadNextInstr, AluIn1, AluSub }, --V
	[0x14] = {"sub y" , LoadNextInstr, AluInY, AluSub },
	[0x15] = {"sub z" , LoadNextInstr, AluInZ, AluSub },
	[0x16] = {"addc"  , LoadNextInstr, AluIn0, AluAddc},
	[0x17] = {"addc y", LoadNextInstr, AluInY, AluAddc},
	[0x18] = {"addc z", LoadNextInstr, AluInZ, AluAddc},
	[0x19] = {"subc"  , LoadNextInstr, AluIn0, AluSubc},
	[0x1A] = {"subc y", LoadNextInstr, AluInY, AluSubc},
	[0x1B] = {"subc z", LoadNextInstr, AluInZ, AluSubc},
	[0x1C] = {"glb"   , LoadNextInstr, AluIn1, AluAnd },
	[0x1D] = {"and y" , LoadNextInstr, AluInY, AluAnd },
	[0x1E] = {"and z" , LoadNextInstr, AluInZ, AluAnd },
	[0x1F] = {"clb"   , LoadNextInstr, AluIn1, AluAndn},
	[0x20] = {"andn y", LoadNextInstr, AluInY, AluAndn},
	[0x21] = {"andn z", LoadNextInstr, AluInZ, AluAndn},
	[0x22] = {"slb"   , LoadNextInstr, AluIn1, AluOr  },
	[0x23] = {"or y"  , LoadNextInstr, AluInY, AluOr  },
	[0x24] = {"or z"  , LoadNextInstr, AluInZ, AluOr  },
	[0x25] = {"tlb"   , LoadNextInstr, AluIn1, AluXor },
	[0x26] = {"xor y" , LoadNextInstr, AluInY, AluXor },
	[0x27] = {"xor z" , LoadNextInstr, AluInZ, AluXor },
	[0x28] = {"shl"   , LoadNextInstr, AluShift, AluShiftXShl1 },
	[0x29] = {"shr"   , LoadNextInstr, AluShift, AluShiftXShr1 },
	[0x2A] = {"rol"   , LoadNextInstr, AluShift, AluShiftXRol1 },
	[0x2B] = {"ror"   , LoadNextInstr, AluShift, AluShiftXRor1 },
	[0x2C] = {"shra"  , LoadNextInstr, AluShift, AluShiftXShra1},
	[0x2D] = {"inv"   , LoadNextInstr, AluIn0, AluInv, AluXor},
	
	-- noop
	[0x2E] = {"nop", LoadNextInstr}, --V
	[0x2F] = {},
	
	-- load and alu ops imm8
	[0x30] = {"ldx imm8" , IncInstr, LoadImm8}, [0x31] = {LoadNextInstr, AluInI, AluMove, AluOutWX}, --V
	[0x32] = {"ldy imm8" , IncInstr, LoadImm8}, [0x33] = {LoadNextInstr, AluInI, AluMove, AluOutWY},
	[0x34] = {"ldz imm8" , IncInstr, LoadImm8}, [0x35] = {LoadNextInstr, AluInI, AluMove, AluOutWZ},
	[0x36] = {"add imm8" , IncInstr, LoadImm8}, [0x37] = {LoadNextInstr, AluInI, AluAdd },
	[0x38] = {"sub imm8" , IncInstr, LoadImm8}, [0x39] = {LoadNextInstr, AluInI, AluSub },
	[0x3A] = {"addc imm8", IncInstr, LoadImm8}, [0x3B] = {LoadNextInstr, AluInI, AluAddc},
	[0x3C] = {"subc imm8", IncInstr, LoadImm8}, [0x3D] = {LoadNextInstr, AluInI, AluSubc},
	[0x3E] = {"and imm8" , IncInstr, LoadImm8}, [0x3F] = {LoadNextInstr, AluInI, AluAnd }, --V
	[0x40] = {"andn imm8", IncInstr, LoadImm8}, [0x41] = {LoadNextInstr, AluInI, AluAndn},
	[0x42] = {"or imm8"  , IncInstr, LoadImm8}, [0x43] = {LoadNextInstr, AluInI, AluOr  },
	[0x44] = {"xor imm8" , IncInstr, LoadImm8}, [0x45] = {LoadNextInstr, AluInI, AluXor },
	
	-- addr arithmetic register
	[0x46] = {"adda x", IncInstr, AluInX, AddrRightA, AddrAdd, MemModeNone, AddrMem0, AddrLeftAddrResult, AddrLeftWA}, [0x47] = {LoadNextInstr},
	
	-- addr bus moves
	[0x48] = {"cla", IncInstr, AddrBusMove, AddrRight0, AddrLeftWA}, [0x49] = {LoadNextInstr}, --V
	[0x4A] = {"lda s", IncInstr, AddrBusMove, AddrRightS, AddrLeftWA}, [0x4B] = {LoadNextInstr},
	[0x4C] = {"lda v", IncInstr, AddrBusMove, AddrRightV, AddrLeftWA}, [0x4D] = {LoadNextInstr},
	[0x4E] = {"sta s", IncInstr, AddrBusMove, AddrRightA, AddrLeftWS}, [0x4F] = {LoadNextInstr}, --V
	[0x50] = {"sta v", IncInstr, AddrBusMove, AddrRightA, AddrLeftWV}, [0x51] = {LoadNextInstr},
	[0x52] = {"lda x", IncInstr, AluInX, AluMove, AluOutWI}, [0x53] = {LoadNextInstr, AddrLeftIAluOut, AluIn0, AluMove, AddrLeftWA},
	
	-- jump and call
	[0x54] = {"jmp a" , AddrRightA, JumpLong             }, [0x55] = {}, --V
	[0x56] = {"call a", AddrRightA, JumpLong, SaveReturnA}, [0x57] = {},
	[0x58] = {"jmp imm16" , IncInstr, LoadImm8}, [0x59] = {IncInstr, LoadImm16}, [0x5A] = {AddrRightT, JumpLong             }, [0x5B] = {},
	[0x5C] = {"call imm16", IncInstr, LoadImm8}, [0x5D] = {IncInstr, LoadImm16}, [0x5E] = {AddrRightT, JumpLong, SaveReturnA}, [0x5F] = {}, --V
	
	-- 16 bit load immediate
	[0x60] = {"lda imm16", IncInstr, LoadImm8}, [0x61] = {IncInstr, LoadImm16A}, [0x62] = {LoadNextInstr}, [0x63] = {}, --V
	
	-- 8 bit loads
	[0x64] = {"ldx (a)", IncInstr, LoadFromA, AluOutWX}, [0x65] = {LoadNextInstr},
	[0x66] = {"ldy (a)", IncInstr, LoadFromA, AluOutWY}, [0x67] = {LoadNextInstr},
	[0x68] = {"ldz (a)", IncInstr, LoadFromA, AluOutWZ}, [0x69] = {LoadNextInstr},
	[0x6A] = {}, [0x6B] = {},
	[0x6C] = {"ldx (imm16)", IncInstr, LoadImm8}, [0x6D] = {IncInstr, LoadImm16}, [0x6E] = {IncInstr, LoadFromT, AluOutWX}, [0x6F] = {LoadNextInstr},
	[0x70] = {"ldy (imm16)", IncInstr, LoadImm8}, [0x71] = {IncInstr, LoadImm16}, [0x72] = {IncInstr, LoadFromT, AluOutWY}, [0x73] = {LoadNextInstr},
	[0x74] = {"ldz (imm16)", IncInstr, LoadImm8}, [0x75] = {IncInstr, LoadImm16}, [0x76] = {IncInstr, LoadFromT, AluOutWZ}, [0x77] = {LoadNextInstr},
	[0x78] = {"ldx (s+imm8)", IncInstr, LoadImm8}, [0x79] = {IncInstr, LoadFromSOffset, AluOutWI}, [0x7A] = {LoadNextInstr, AluInI, AluMove, AluOutWX}, [0x7B] = {},
	[0x7C] = {"ldy (s+imm8)", IncInstr, LoadImm8}, [0x7D] = {IncInstr, LoadFromSOffset, AluOutWI}, [0x7E] = {LoadNextInstr, AluInI, AluMove, AluOutWY}, [0x7F] = {},
	[0x80] = {"ldz (s+imm8)", IncInstr, LoadImm8}, [0x81] = {IncInstr, LoadFromSOffset, AluOutWI}, [0x82] = {LoadNextInstr, AluInI, AluMove, AluOutWZ}, [0x83] = {},
	[0x84] = {"pop x", IncInstr, PopI}, [0x85] = {LoadNextInstr, AluInI, AluMove, AluOutWX}, --V
	[0x86] = {"pop y", IncInstr, PopI}, [0x87] = {LoadNextInstr, AluInI, AluMove, AluOutWY}, --V
	[0x88] = {"pop z", IncInstr, PopI}, [0x89] = {LoadNextInstr, AluInI, AluMove, AluOutWZ}, --V
	
	-- 8 bit stores
	[0x8A] = {"stx (a)", IncInstr, AluInX, StoreToA}, [0x8B] = {LoadNextInstr},
	[0x8C] = {"sty (a)", IncInstr, AluInY, StoreToA}, [0x8D] = {LoadNextInstr},
	[0x8E] = {"stz (a)", IncInstr, AluInZ, StoreToA}, [0x8F] = {LoadNextInstr},
	[0x90] = {"stx (imm16)", IncInstr, LoadImm8}, [0x91] = {IncInstr, LoadImm16}, [0x92] = {IncInstr, AluInX, StoreToT}, [0x93] = {LoadNextInstr}, --V
	[0x94] = {"sty (imm16)", IncInstr, LoadImm8}, [0x95] = {IncInstr, LoadImm16}, [0x96] = {IncInstr, AluInY, StoreToT}, [0x97] = {LoadNextInstr},
	[0x98] = {"stz (imm16)", IncInstr, LoadImm8}, [0x99] = {IncInstr, LoadImm16}, [0x9A] = {IncInstr, AluInZ, StoreToT}, [0x9B] = {LoadNextInstr},
	[0x9C] = {"stx (s+imm8)", IncInstr, LoadImm8}, [0x9D] = {IncInstr, OffsetSToT, MemOff}, [0x9E] = {IncInstr, AluInX, StoreToT}, [0x9F] = {LoadNextInstr},
	[0xA0] = {"sty (s+imm8)", IncInstr, LoadImm8}, [0xA1] = {IncInstr, OffsetSToT, MemOff}, [0xA2] = {IncInstr, AluInY, StoreToT}, [0xA3] = {LoadNextInstr},
	[0xA4] = {"stz (s+imm8)", IncInstr, LoadImm8}, [0xA5] = {IncInstr, OffsetSToT, MemOff}, [0xA6] = {IncInstr, AluInZ, StoreToT}, [0xA7] = {LoadNextInstr},
	[0xA8] = {"push x", IncInstr, AluInX, Push8Bit}, [0xA9] = {LoadNextInstr}, --V
	[0xAA] = {"push y", IncInstr, AluInY, Push8Bit}, [0xAB] = {LoadNextInstr},
	[0xAC] = {"push z", IncInstr, AluInZ, Push8Bit}, [0xAD] = {LoadNextInstr},
	
	-- relative jump
	[0xAE] = {"jr imm8", IncInstr, LoadImm8}, [0xAF] = {JumpRelative},
	
	-- 16 bit loads and stores
	[0xB0] = {"lda (a)", IncInstr, LoadFromA, AluOutWI}, [0xB1] = {IncInstr, LoadFromAPlus1, AddrLeftIAluOut, AddrLeftWA}, [0xB2] = {LoadNextInstr}, [0xB3] = {},
	[0xB4] = {"pop a", IncInstr, Pop2I}, [0xB5] = {IncInstr, Pop216, AddrLeftWA}, [0xB6] = {LoadNextInstr}, [0xB7] = {}, --V
	[0xB8] = {"lda (s+imm8)", IncInstr, LoadImm8}, [0xB9] = {IncInstr, LoadFromSOffset, AluOutWI}, [0xBA] = {IncInstr, LoadFromSOffsetPlus1, AddrLeftIAluOut, AddrLeftWA}, [0xBB] = {LoadNextInstr},
	[0xBC] = {"push a", IncInstr, AluInAH, Push8Bit}, [0xBD] = {IncInstr, AluInAL, Push8Bit}, [0xBE] = {LoadNextInstr}, [0xBF] = {}, --V
	[0xC0] = {"lda (imm16)", IncInstr, LoadImm8}, [0xC1] = {IncInstr, LoadImm16}, [0xC2] = {IncInstr, LoadFromT, AluOutWI}, [0xC3] = {IncInstr, LoadFromTPlus1, AddrLeftIAluOut, AddrLeftWA}, [0xC4] = {LoadNextInstr}, [0xC5] = {}, [0xC6] = {}, [0xC7] = {},
	[0xC8] = {"sta (imm16)", IncInstr, LoadImm8}, [0xC9] = {IncInstr, LoadImm16}, [0xCA] = {IncInstr, AluInAL, StoreToT}, [0xCB] = {IncInstr, AluInAH, StoreToTPlus1}, [0xCC] = {LoadNextInstr}, [0xCD] = {}, [0xCE] = {}, [0xCF] = {},
	[0xD0] = {"sta (s+imm8)", IncInstr, LoadImm8}, [0xD1] = {IncInstr, OffsetSToT, MemOff}, [0xD2] = {IncInstr, AluInAL, StoreToT}, [0xD3] = {IncInstr, AluInAH, StoreToTPlus1}, [0xD4] = {LoadNextInstr}, [0xD5] = {}, [0xD6] = {}, [0xD7] = {},
	
	-- address arithmetic
	[0xD8] = {"adda imm8", IncInstr, LoadImm8}, [0xD9] = {IncInstr, AluInI, AddrRightA, AddrAdd, MemOff, AddrLeftAddrResult, AddrLeftWA}, [0xDA] = {LoadNextInstr}, [0xDB] = {},
	[0xDC] = {"adds imm8", IncInstr, LoadImm8}, [0xDD] = {IncInstr, AluInI, AddrRightS, AddrAdd, MemOff, AddrLeftAddrResult, AddrLeftWS}, [0xDE] = {LoadNextInstr}, [0xDF] = {},
	
	-- conditional jumps
	[0xE0] = {"jz imm8" , IncInstr, LoadImm8}, [0xE1] = {JumpRelativeCond, CondZ, CondCheck},
	[0xE2] = {"jnz imm8", IncInstr, LoadImm8}, [0xE3] = {JumpRelativeCond, CondZ,          }, --V
	[0xE4] = {"jl imm8" , IncInstr, LoadImm8}, [0xE5] = {JumpRelativeCond, CondL, CondCheck},
	[0xE6] = {"jnl imm8", IncInstr, LoadImm8}, [0xE7] = {JumpRelativeCond, CondL,          },
	[0xE8] = {"jg imm8" , IncInstr, LoadImm8}, [0xE9] = {JumpRelativeCond, CondG, CondCheck},
	[0xEA] = {"jng imm8", IncInstr, LoadImm8}, [0xEB] = {JumpRelativeCond, CondG,          },
	[0xEC] = {"jc imm8" , IncInstr, LoadImm8}, [0xED] = {JumpRelativeCond, CondC, CondCheck},
	[0xEE] = {"jnc imm8", IncInstr, LoadImm8}, [0xEF] = {JumpRelativeCond, CondC,          },
	
	-- interrupt
	[0xF0] = {"brk" , AddrRightV, JumpLong, SaveReturnVInc, IntFlagSet  }, -- NYI
	[0xF1] = {"int" , AddrRightV, JumpLong, SaveReturnV   , IntFlagSet  }, -- NYI
	[0xF2] = {"iret", AddrRightV, JumpLong, SaveReturnVInc, IntFlagClear}, -- NYI
	[0xF3] = {}, [0xF4] = {}, [0xF5] = {}, [0xF6] = {}, [0xF7] = {}, [0xF8] = {}, [0xF9] = {}, [0xFA] = {}, [0xFB] = {}, [0xFC] = {}, [0xFD] = {}, [0xFE] = {}, [0xFF] = {},
}

return ucode
