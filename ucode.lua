
local function ucode_expand_recursive(code, val)
	if type(code)=="number" then -- actual value
		table.insert(val, code)
	elseif type(code)=="table" then -- collection of values
		for code_idx, code_val in ipairs(code) do
			ucode_expand_recursive(code_val, val)
		end
	elseif type(code)=="string" then
		if val.mnemonic then error("code has multiple mnemonics ("..code.." and "..val.mnemonic..")") end
		val.mnemonic = code
	else
		error("invalid type "..type(code).." in ucode")
	end
end
local function ucode_check(code)
	local sigs = {}
	for sig_idx, sig in ipairs(code) do
		if sigs[sig] then
			error("signal "..sig.." present multiple times in code "..string.format("%02X", code.opcode).." "..(code.mnemonic or ""))
		end
		sigs[sig] = true
	end
end
local function ucode_expand(code, opcode)
	local val = {
		opcode = opcode,
	}
	ucode_expand_recursive(code, val)
	ucode_check(val)
	return val
end

function Ucode_process(ucode)
	local ucode2 = {}
	for opcode = 0, 0xFF do
		if not ucode[opcode] then error("no ucode for opcode "..string.format("%02X", opcode)) end
		local code = ucode_expand(ucode[opcode], opcode)
		ucode2[opcode] = code
	end
	return ucode2
end

function Reverse_8bit(n)
	local r = 0
	for i = 7, 0, -1 do
		local p = math.pow(2, i)
		if n >= p then
			n = n - p
			r = r + math.pow(2, 7-i)
		end
	end
	return r
end
